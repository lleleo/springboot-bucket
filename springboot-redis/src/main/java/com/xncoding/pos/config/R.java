package com.xncoding.pos.config;

/**
 * 文件名：R.java
 *
 * @create 2019-11-22 10:00
 * <p>
 * <p>
 * <p>
 */
public class R {
    private int code;
    private String message;
    private Object data;
    public R(){
    }

    public static R success(String msg,Object data){
        return new R(AppConst.SUCCESS_CODE,msg,data);
    }

    public static R success(String msg){
        return R.success(msg,null);
    }
    public static R success(){
        return success(null,null);
    }
    public static R fail(int code,String msg,Object data){
        return new R(code,msg,data);
    }
    public static R fail(int code,String msg){
        return R.fail(code,msg,null);
    }

    public static R fail(String msg){
        return R.fail(msg,null);
    }
    public static R fail(String msg,Object data){
        return R.fail(AppConst.FAIL_CODE,msg,data);
    }


    private R(int code, String msg, Object data){
        this.code = code;
        this.message = msg;
        this.data = data;
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
