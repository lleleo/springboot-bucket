package com.xncoding.pos.config.norepeat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoRepeatSubmit {

    /**
     * 设置请求锁定时间 秒
     * @return
     */
    int lockTime() default 10;
    /**
     * @Author: Dibbing pan
     * @Date: 2019-11-25 14:44
     * @Description : 方法结束,是否自动解锁
     * @return:
     */
    boolean autoUnLock() default true;

//    String[] paramNames();

    TimeUnit timeUnit() default TimeUnit.SECONDS;

}
