package com.xncoding.pos.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * 文件名：AppConst.java
 * @create 2019-11-25 15:33
 * @create by Dibbing pan
 *
 * @Description: 系统常量定义类
 *
 */
public class AppConst {
    public static final String FileSeparator = File.separator;

    public static final int FAIL_CODE = -1;
    public static final int SUCCESS_CODE = 1;

    public static String fileRootpath;
    public static Properties flowTypes;
    //
    public static String FlowExportTemplateRootPath;
    public static String FlowExportOutRootPath;

    static {
        fileRootpath=System.getProperties().getProperty("user.dir")+ FileSeparator+"files"+FileSeparator;
        flowTypes = new Properties();
        FlowExportTemplateRootPath=fileRootpath+"export_template"+FileSeparator;
        FlowExportOutRootPath=fileRootpath+"export_out"+FileSeparator;
    }
}
