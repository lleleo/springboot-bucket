package com.xncoding.pos.user.entity;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xncoding.pos.user.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
