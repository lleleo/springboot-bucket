package com.xncoding.jwt.api;

import com.danga.MemCached.MemCachedClient;
import com.xncoding.jwt.config.web.swagger.SwaggerTagConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "请求接口类", tags =  SwaggerTagConst.memcached.FLOW, description = "登录获取Token")
@RestController
@RequestMapping("/mm")
public class MemcachedController {
    @Autowired
    private MemCachedClient memCachedClient;

    @ApiOperation("综合测试")
    @GetMapping("/test")
    public String test() {
        // 放入缓存
        boolean flag = memCachedClient.set("username", "kalychen");
        System.out.println(flag);
        // 取出缓存
        Object value = memCachedClient.get("username");
        System.out.println(value);
        return String.valueOf(value);
    }

    @ApiOperation("写缓存")
    @PutMapping("/put")
    public Boolean put(String k, String v) {
        // 放入缓存
        boolean flag = memCachedClient.set(k, v);
        return flag;
    }

    @ApiOperation("读缓存")
    @GetMapping("/get")
    public String get(String k) {
        // 取出缓存
        Object value = memCachedClient.get(k);
        return String.valueOf(value);
    }

}

