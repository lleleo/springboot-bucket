EhCache：是一个纯Java的进程内缓存框架，具有快速、精干等特点，是Hibernate、MyBatis默认的缓存提供。
（备注：虽然EhCache3支持到了分布式，但它还是基于Java进程的缓存）

Guava：它是Google Guava工具包中的一个非常方便易用的本地化缓存实现，基于LRU算法实现，支持多种缓存过期策略。它出现得非常早，有点廉颇老矣之感~

Caffeine：是使用Java8对Guava缓存的重写版本，在Spring5中将取代了Guava，支持多种缓存过期策略。 
稳定，健壮,简单,秀珍,轻量,好扩展