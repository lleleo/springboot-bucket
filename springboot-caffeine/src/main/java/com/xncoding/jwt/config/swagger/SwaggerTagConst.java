package com.xncoding.jwt.config.swagger;

/**
 * 文件名：SwaggerTagConst.java
 * @create 2020-05-08 14:18
 * @create by Dibbing pan
 *
 * @Description: swaggerTAG枚举类
 *
 */
public class SwaggerTagConst {
    public static class SYSTEM {
        public static final String PUBLIC = "系统通用接口";
        public static final String ATTACHMENT = "系统附件";
        public static final String USER = "用户";
        public static final String exception = "异常";
    }
    /**
     *  第三方系统管理
     */
    public static class FLOW {
        public static final String FLOW="流程服务";
        public static final String FLOW_TEST="流程测试服务";
    }
}
