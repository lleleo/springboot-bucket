package com.xncoding.jwt.service.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 后台管理用户表
 *
 * @author 熊能
 * @version 1.0
 * @since 2018/01/02
 */
@TableName(value = "sys_user")
@Data
public class User extends Model<User> {

private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value="user_id", type= IdType.AUTO)
    private Integer id;
    /**
     * 账号
     */
    private String userName;
    /**
     * 名字
     */
    private String code;
    /**
     * 密码
     */
    private String ensp;
    /**
     * md5密码盐
     */
    private String st;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 备注
     */
    private String email;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
