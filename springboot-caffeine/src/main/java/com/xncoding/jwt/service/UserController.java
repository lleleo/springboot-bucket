package com.xncoding.jwt.service;

import com.xncoding.jwt.config.swagger.SwaggerTagConst;
import com.xncoding.jwt.service.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags =  SwaggerTagConst.SYSTEM.USER)
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource private UserService userService;

    @ApiOperation("根据用户id查询用户, 使用缓存框架测试")
    @ApiImplicitParam(name = "id", defaultValue = "1")
    @ResponseBody
    @GetMapping("findById")
    public User findById(Integer id) {
        return userService.findById(id);
    }

    @ApiOperation("清空缓存")
    @ResponseBody
    @DeleteMapping("clearCache")
    public void clearCache(Integer id) {
        userService.clearCache(id);
    }
}
