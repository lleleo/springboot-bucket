package com.xncoding.starter.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("example")
public class ExampleProperty {
    private Boolean enabled;
    private String prefix;
    private String suffix;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
