package com.xncoding.starter.config;

import com.xncoding.starter.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.Resource;

/**
 * ExampleAutoConfigure
 *
 * @author XiongNeng
 * @version 1.0
 * @since 2018/2/28
 */
@Configuration
@Import(ExampleProperty.class)
public class StarterConfigure {
    private final ExampleProperty exampleProperty;
    @Autowired
    public StarterConfigure(ExampleProperty exampleProperty) {
        this.exampleProperty = exampleProperty;
    }
    @Bean
    @ConditionalOnProperty(prefix = "example", value = "enabled",havingValue = "true")
    ExampleService exampleService (){
        ExampleService exampleService =  new ExampleService(exampleProperty.getPrefix(), exampleProperty.getSuffix());
        System.out.println("-----------springboot-starter---------------"+exampleService.wrap("-abc-"));;
        return exampleService;
    }
    /*
    @Value("${example.prefix:p}")
    private String prefix;
    @Value("${example.suffix:s}")
    private String suffix;

    @Bean
    @ConditionalOnProperty(prefix = "example", value = "enabled",havingValue = "true")
    ExampleService exampleService (){
        ExampleService exampleService =  new ExampleService(prefix, suffix);
        System.out.println("-----------springboot-starter---------------"+exampleService.wrap("-abc-"));;
        return exampleService;
    }
    */
}
