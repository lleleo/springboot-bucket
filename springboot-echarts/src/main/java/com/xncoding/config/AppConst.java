package com.xncoding.config;

import java.io.File;

public class AppConst {

    public static String fileRootpath;

    public static final String projectName="springboot-echarts";


    static {
        String path=System.getProperties().getProperty("user.dir") + File.separator;
        if(path.contains(projectName)) {
            fileRootpath = path + "imags" + File.separator;
        }else {
            fileRootpath = path  + projectName + File.separator + "imags" + File.separator;
        }
    }
}
