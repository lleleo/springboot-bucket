package com.xncoding.jwt.config.web.swagger;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xncoding.jwt.config.AppConst;
import io.swagger.annotations.Api;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * [ 根据SwaggerTagConst内部类动态生成Swagger group ]
 *
 */
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
@Profile({"dev", "test", "prod"})
public class SmartSwaggerDynamicGroupConfig implements EnvironmentAware, BeanDefinitionRegistryPostProcessor {
    /**
     * 分组名称
     */
    private String apiGroupName;
    /**
     * 文档标题
     */
    private String title;

    private String version;
    private String license;
    private boolean enable = false;
    private String serviceUrl;
    private String contact;
    private String contactUrl;
    private String contactEmail;
    /**
     * 文档描述
     */
    private String description;

    /**
     * controller 包路径
     */
    private String packAge;

    private int groupIndex = 0;

    private String groupName = "default";

    private List<String> groupList = Lists.newArrayList();

    private Map<String, List<String>> groupMap = Maps.newHashMap();

    @Override
    public void setEnvironment(Environment environment) {
        this.enable = Boolean.parseBoolean(environment.getProperty("swagger.enable"));
        this.license = environment.getProperty("swagger.license");
        this.apiGroupName = environment.getProperty("swagger.apiGroupName");
        this.title = environment.getProperty("swagger.title");
        this.description = environment.getProperty("swagger.description");
        this.version = environment.getProperty("swagger.version");
        this.serviceUrl = environment.getProperty("swagger.serviceUrl");
        this.packAge = environment.getProperty("swagger.packAge");
        this.contact = environment.getProperty("swagger.contact");
        this.contactUrl = environment.getProperty("swagger.contactUrl");
        this.contactEmail = environment.getProperty("swagger.contactEmail");
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        this.groupBuild();
        for (Map.Entry<String, List<String>> entry : groupMap.entrySet()) {
            String group = entry.getKey();
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(Docket.class, this :: baseDocket);
            BeanDefinition beanDefinition = builder.getRawBeanDefinition();
            registry.registerBeanDefinition(group + "Api", beanDefinition);
        }
    }

    private void groupBuild() {
        Class clazz = SwaggerTagConst.class;
        Class[] innerClazz = clazz.getDeclaredClasses();
        for (Class cls : innerClazz) {
            String group = cls.getSimpleName();
            List<String> apiTags = Lists.newArrayList();
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                boolean isFinal = Modifier.isFinal(field.getModifiers());
                if (isFinal) {
                    try {
                        apiTags.add(field.get(null).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            groupList.add(group);
            groupMap.put(group, apiTags);
        }
    }
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(serviceApiInfo())
                .enable(enable)
                //分组名称
                .groupName(groupName)
                .forCodeGeneration(true)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage(packAge))
                .apis(getControllerPredicate())
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
        return docket;
    }
    private Docket baseDocket() {
        // 请求类型过滤规则
        Predicate<RequestHandler> controllerPredicate = getControllerPredicate();
        // controller 包路径
        Predicate<RequestHandler> controllerPackage = RequestHandlerSelectors.basePackage(packAge);
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .enable(enable)
                .forCodeGeneration(true)
                .select()
                .apis(controllerPackage)
                .apis(controllerPredicate)
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.serviceApiInfo())
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());

//        Docket docket=new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
//                //分组名称
//                .groupName("2.X版本")
//                .select()
//                //这里指定Controller扫描包路径
//                .apis(RequestHandlerSelectors.basePackage("com.swagger.bootstrap.ui.demo.new2"))
//                .paths(PathSelectors.any())
//                .build();
//        return docket;
    }

    private List<ApiKey> securitySchemes() {
        List<ApiKey> apiKeyList= new ArrayList<>();
        apiKeyList.add(new ApiKey("x-access-token", AppConst.USER_ID_HTTP_HEAD_KEY, "header"));
        return apiKeyList;
    }

    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts=new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .forPaths(PathSelectors.any())
                        .build());
        return securityContexts;
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences=new ArrayList<>();
        securityReferences.add(new SecurityReference("x-access-token", authorizationScopes));
        return securityReferences;
    }

    private Predicate<RequestHandler> getControllerPredicate() {
        if(groupList.size()<groupIndex){
            return null;
        }
        groupName = groupList.get(groupIndex);
        List<String> apiTags = groupMap.get(groupName);
        Predicate<RequestHandler> methodPredicate = (input) -> {
            Api api = null;
            Optional<Api> apiOptional = input.findControllerAnnotation(Api.class);
            if (apiOptional.isPresent()) {
                api = apiOptional.get();
            }
            List<String> tags = Arrays.asList(api.tags());
            if (api != null && apiTags.containsAll(tags)) {
                return true;
            }
            return false;
        };
        groupIndex++;
        // 扫描注解了API的controller类
        return Predicates.and(RequestHandlerSelectors.withClassAnnotation(Api.class), methodPredicate);
    }

    private ApiInfo serviceApiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .version(version)
                .license(license)
                .contact(new Contact(contact, contactUrl, ""))
                .termsOfServiceUrl(serviceUrl)
                .build();
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
