package com.xncoding.jwt.config.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class OpenBrowserHandler implements CommandLineRunner {
    @Value("${server.port}")
    private int serverPort;

    @Override
    public void run(String... args) {
        try {
            Runtime.getRuntime().exec("cmd  /c  start   http://127.0.0.1:" + serverPort + "/doc.html");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}