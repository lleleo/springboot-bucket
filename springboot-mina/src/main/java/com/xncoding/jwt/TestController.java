package com.xncoding.jwt;

import com.xncoding.jwt.config.handler.ServerHandler;
import com.xncoding.jwt.config.web.swagger.SwaggerTagConst;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@Api(tags =  SwaggerTagConst.MINA.TEST)
@RestController
@RequestMapping
public class TestController {
    @Resource private ServerHandler serverHandler;

    @GetMapping
    public boolean sendMsg(){
        return serverHandler.sendToAllSession("123");
    }
}
