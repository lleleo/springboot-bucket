package com.xncoding.jwt.config.sse;

import org.springframework.context.ApplicationEvent;

/**
 * @author Dibbing Pan
 * @date 2019/8/28
 */
public class PayCompletedEvent extends ApplicationEvent {
    private Long payRecordId;

    public PayCompletedEvent(Object source, Long payRecordId) {
        super(source);
        this.payRecordId = payRecordId;
    }

    public Long getPayRecordId() {
        return payRecordId;
    }

    public void setPayRecordId(Long payRecordId) {
        this.payRecordId = payRecordId;
    }
}