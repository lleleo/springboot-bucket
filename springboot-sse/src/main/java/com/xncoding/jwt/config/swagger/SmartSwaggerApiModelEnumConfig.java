package com.xncoding.jwt.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

/**
 * [ 对于枚举类进行swagger注解，与前端的vue-enum相匹配 ]
 *
 */
@Configuration
@Profile({"dev", "test", "prod"})
public class SmartSwaggerApiModelEnumConfig {

    @Bean
    @Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1)
    public SmartSwaggerApiModelEnumPlugin swaggerEnum(){
        return new SmartSwaggerApiModelEnumPlugin();
    }
}
