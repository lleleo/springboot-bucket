package com.xncoding.jwt.api;

import com.xncoding.jwt.config.web.SwaggerTagConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * 文件名：FlowController.java
 *
 * @create 2020-05-09 13:19
 * <p>
 * <p>
 */
@Api(tags =  SwaggerTagConst.FLOW.FLOW)
@RestController
@RequestMapping("/flow")
public class FlowController {



    @ApiOperation("待办数")
    @ResponseBody
    @GetMapping("/getAuditProveSize")
    public Integer getAuditProveSize(){
        return 2;
    }

    @ApiImplicitParam("流程分类")
    @GetMapping("/getTypeList")
    @ResponseBody
    public Object getTypeList(){
        return "11111";
    }



    @ApiOperation("启动流程flowId")
    @PostMapping("/startProcess")
    @ResponseBody
    @ApiImplicitParam(name = "flowId",value = "流程记录id",required = true)
    public boolean startProcess(String flowId){
        return true;
    }
    @ApiOperation("启动流程_业务id和流程类型启动流程")
    @PostMapping("/startProcessByBizId")
    @ResponseBody
    @ApiImplicitParam(name = "bizId",value = "业务id",required = true)
    public Boolean startProcessByBizId(String bizId,String typeId){
        return true;
    }
}
