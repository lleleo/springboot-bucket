package com.xncoding.jwt.order.entity;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface OrderMapper {

    OrderDO selectById(@Param("id") Long id);

    OrderDO selectByIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);

    List<OrderDO> selectListByUserId(@Param("userId") Integer userId);

    void insert(OrderDO order);

    List<Map<String,String>> getCountOrder();

    List<Map<String,String>> groupUserCount();

    List<Map<String,String>> groupProductCount();

    List<Map<String,String>> productUserCount();


}
