package com.xncoding.jwt.order.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OrderDO {
    /**
     CREATE TABLE `orders_7` (
     `id`  bigint NOT NULL ,
     `user_id`  int NULL ,
     `test`  date NULL ,
     `product_id`  varchar(32) NULL ,
     PRIMARY KEY (`id`)
     )
     ;

     */
    /**
     * 订单编号
     */
    private Long id;
    /**
     * 用户编号
     */
    private Integer userId;

    private Date test;

    private String productId;
}
