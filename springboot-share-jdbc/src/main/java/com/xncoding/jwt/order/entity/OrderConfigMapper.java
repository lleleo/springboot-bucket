package com.xncoding.jwt.order.entity;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface OrderConfigMapper {
    OrderConfigDO selectById(@Param("id") Integer id);

    void insert(OrderConfigDO configDO);
}
