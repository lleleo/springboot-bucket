package com.xncoding.jwt.order;

import com.xncoding.jwt.config.web.SwaggerTagConst;
import com.xncoding.jwt.order.entity.OrderConfigDO;
import com.xncoding.jwt.order.entity.OrderConfigMapper;
import com.xncoding.jwt.order.entity.OrderDO;
import com.xncoding.jwt.order.entity.OrderMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags =  SwaggerTagConst.WORK.ORDER,value = "123")
@RestController
@RequestMapping
public class OrderController {
    @Resource
    private OrderMapper orderMapper;
    @Resource
    private OrderConfigMapper orderConfigMapper;

    @ApiOperation("根据id查询订单")
    @GetMapping
    @ResponseBody
    public Object getById(Long i){
        return orderMapper.selectById(i);
    }

    /***
     * 查询分库分表数据, 最好条件带上分库分表策略字段, 否则每个表都要查询一次.
     * @param userId
     * @param i
     * @return
     */
    @ApiOperation("根据id和用户id查询订单")
    @GetMapping("getByIdAndUserId")
    @ResponseBody
    public Object getByIdAndUserId(Long userId,Long i){
        return orderMapper.selectByIdAndUserId(i,userId);
    }

    @ApiOperation("根据用户id查询订单")
    @GetMapping("getByUserId")
    @ResponseBody
    public Object getByUserId(int uid){
        return orderMapper.selectListByUserId(uid);
    }

    @ApiOperation("保存订单")
    @PostMapping
    @ResponseBody
    public Object save(@RequestBody OrderDO orderDO){
        orderMapper.insert(orderDO);
        return orderDO;
    }
    @ApiOperation("保存订单配置")
    @PostMapping("saveConfig")
    @ResponseBody
    public Object saveConfig(@RequestBody OrderConfigDO configDO){
        orderConfigMapper.insert(configDO);
        return configDO;
    }

    @ApiOperation("统计总数")
    @GetMapping("count")
    public Object getCountOrder(){
        return orderMapper.getCountOrder();
    }

    @ApiOperation("用户分组统计")
    @GetMapping("groupUserCount")
    public Object groupUserCount(){
        return orderMapper.groupUserCount();
    }
    @ApiOperation("商品用户分组统计")
    @GetMapping("productUserCount")
    public Object productUserCount(){
        return orderMapper.productUserCount();
    }

    @ApiOperation("商品分组统计")
    @GetMapping("groupProductCount")
    public Object groupProductCount(){
        return orderMapper.groupProductCount();
    }
}
