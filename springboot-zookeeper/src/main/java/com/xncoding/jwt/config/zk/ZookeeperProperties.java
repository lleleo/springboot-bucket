package com.xncoding.jwt.config.zk;


import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "zookeeper")
@Data
@ToString
public class ZookeeperProperties {
    private boolean enabled;
    private String server;
    private String namespace;
    private String digest;

    // 允许opc读取程序开启线程个数, opc server 平分
    private int sessionTimeoutMs;

    private int connectionTimeoutMs;

    private int maxRetries;

    private int baseSleepTimeMs;

}
