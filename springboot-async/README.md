## 异步线程池

演示在SpringBoot中如何使用异步线程池

## 测试用例

`com.xncoding.pos.ApplicationTests.java`

async方法调用了其他spring的bean时，springboot项目会启动时报错。
spring加载bean，按名称加载，定义class时，定义在字母排序前面

## 许可证

Copyright (c) 2018 Xiong Neng

基于 MIT 协议发布: <http://www.opensource.org/licenses/MIT>
