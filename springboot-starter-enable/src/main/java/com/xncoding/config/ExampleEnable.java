package com.xncoding.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ExampleAutoConfigure.class)
public @interface ExampleEnable {

}
