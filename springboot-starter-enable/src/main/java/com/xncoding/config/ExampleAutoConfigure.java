package com.xncoding.config;

import com.xncoding.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExampleAutoConfigure {

    @Value("${example.enabled:false}")
    private Boolean enabled;
    @Bean
    public UserService userService(){
        UserService userService = new UserService("tst"+enabled);
        userService.test();
        return userService;
    }
}
