package com.xncoding.jwt.config;

import java.io.File;
import java.util.Properties;

public class AppConst {
    public static final String seprater = ",";
    public static final String FileSeparator = File.separator;

    public static final String USER_ID_HTTP_HEAD_KEY= "USER_ID";// 回调地址token的参数名称

    public static String fileRootpath;

    static {
        fileRootpath=System.getProperties().getProperty("user.dir")+ FileSeparator+"files"+FileSeparator;
    }

}
