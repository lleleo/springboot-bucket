package com.xncoding.jwt;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件名：IndexController.java
 *
 * @create 2020-07-30 14:19
 * <p>
 * <p>
 */
@RestController
@RequestMapping("/")
public class IndexController {
    /**
     * @Author: Dibbing pan
     * @Date: 2020-07-30 14:21
     * @Description : http://localhost:8080/index     https://localhost/index
     * @return:
     */
    @GetMapping("/index")
    @ResponseBody
    public String index(){
        return "hello,springBoot";
    }
}
