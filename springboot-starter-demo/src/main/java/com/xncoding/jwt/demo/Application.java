package com.xncoding.jwt.demo;

import com.xncoding.config.ExampleEnable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@ExampleEnable
@SpringBootApplication
public class Application{
    public static void main( String[] args )    {
        SpringApplication.run(Application.class, args);
        System.out.println( "Hello World!" );
    }
}
